import type { IAddOptions } from 'pixi.js';

export declare type TAssetMap = IAddOptions[];

export declare type TSprites = {
    [key: string]: Sprite;
};
