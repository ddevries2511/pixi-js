import './style/app.scss';
import { Sprite } from 'pixi.js';

import Game from './shared/Game.class';
import Loader from './shared/Loader.class';
import EventEmitter from './shared/EventEmitter.class';

import Menu from './Menu/Menu.class';

import Player from './Player/Player.class';
import playerAssets from './Player/assets';

import Enemy from './Object/Target/Enemy/Enemy.class';
import enemyAssets from './Object/Target/Enemy/assets';

import { hitTest } from './Object/utils';
import type { TAssetMap } from './types';

const loader = Loader;

// Load all assets
const assets: TAssetMap = [...playerAssets, ...enemyAssets];
for (const asset of assets) {
    loader.shared.add(asset);
}

// resources is an object where the key is the name of the resource loaded and the value is the resource object.
// They have a couple default properties:
// - `url`: The URL that the resource was loaded from
// - `error`: The error that happened when trying to load (if any)
// - `data`: The raw data that was loaded
// also may contain other properties based on the middleware that runs.
loader.shared.load((_loader, resources) => {
    for (const key in resources) {
        const resource = resources[key];
        loader.sprites[key] = new Sprite(resource.texture);
    }
});

loader.shared.onComplete.add(() => {
    console.info('Assets loaded', loader.sprites);

    new Menu();

    EventEmitter.once('gameStarted', () => {
        // Init players
        const player1 = new Player({ gamertag: 'player1' });
        player1.spaceship.init();

        // Let's cheat a bit and upgrade to max rank
        player1.playerRank = 'rank_3';

        // Create enemies
        const enemies = [
            new Enemy(),
            new Enemy(),
            new Enemy(),
            new Enemy(),
            new Enemy(),
        ];

        // Set enemy positions
        enemies.forEach((enemy, i) => {
            enemy.init();
            enemy.setPositionX(i * enemy.width);
            enemy.setPositionY(i * enemy.height);
        });

        Game.app.ticker.add(() => {
            // bullet hit detection
            const bullets = player1.spaceship.shotBullets;
            for (let bi = 0; bi < bullets.length; bi++) {
                const bullet = bullets[bi];

                for (const enemy of enemies) {
                    if (enemy.isDestroyed) continue;

                    const hit = hitTest(bullet, enemy.target);

                    if (!hit) continue;

                    enemy.destroy();
                    enemies.splice(enemies.indexOf(enemy), 1);

                    if (bullet) player1.spaceship.destroyBulletAt(bi);
                }
            }

            if (!enemies.length) {
                console.info('You win!');
            }
        });
    });
});
