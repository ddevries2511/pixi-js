import type { IApplicationOptions } from 'pixi.js';

export const resolution: IApplicationOptions['resolution'] =
    window.devicePixelRatio || 1;
