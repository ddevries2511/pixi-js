import { sound } from '@pixi/sound';
import EventEmitter from '../shared/EventEmitter.class';
import Spaceship from './Spaceship.class';

type TPlayerOptions = {
    gamertag: string;
    /**
     * @default 'rank_1'
     */
    rank?: TRank;
};

export type TStats = {
    vehicle: {
        velocity: number;
    };
    weapons: TWeapons;
};

type TWeapons = {
    canon: TWeaponStats;
};

type TWeaponStats = {
    velocity: number;
    damage: number;
};

type TRank = 'rank_1' | 'rank_2' | 'rank_3';

type TRankStats = {
    [key in TRank]: TStats;
};

export type TRankChangeEvent = {
    rank: TRank;
    stats: TStats;
};

class Player {
    private gamertag: TPlayerOptions['gamertag'];
    private stats: TRankStats;
    private rank: TRank;

    public spaceship: Spaceship;

    constructor({ gamertag, rank = 'rank_1' }: TPlayerOptions) {
        this.gamertag = gamertag;

        this.rank = rank;

        this.stats = {
            rank_1: {
                vehicle: {
                    velocity: 1,
                },
                weapons: {
                    canon: {
                        velocity: 2,
                        damage: 2,
                    },
                },
            },
            rank_2: {
                vehicle: {
                    velocity: 2,
                },
                weapons: {
                    canon: {
                        velocity: 4,
                        damage: 4,
                    },
                },
            },
            rank_3: {
                vehicle: {
                    velocity: 3,
                },
                weapons: {
                    canon: {
                        velocity: 6,
                        damage: 6,
                    },
                },
            },
        };

        this.spaceship = new Spaceship(this.playerStats);

        // Preload sound effects
        sound.add('lvlUp', {
            url: 'assets/sounds/PM_FN_Events_LvlUps_PowerUps_56.mp3',
            preload: true,
        });
    }

    private emitRankChanged() {
        const payload: TRankChangeEvent = {
            rank: this.rank,
            stats: this.playerStats,
        };
        EventEmitter.emit('playerRankChanged', payload);
    }

    public get playerGamertag() {
        return this.gamertag;
    }

    public get playerStats() {
        return this.stats[this.rank];
    }

    public get playerRank() {
        return this.rank;
    }

    public set playerRank(rank: TRank) {
        this.rank = rank;
        this.emitRankChanged();
        // Play 'level up' sound effect
        sound.play('lvlUp');
    }
}

export default Player;
