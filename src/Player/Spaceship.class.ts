import { Container, Sprite } from 'pixi.js';
import { sound } from '@pixi/sound';
import Keyboard from 'pixi.js-keyboard';

import Game from '../shared/Game.class';
import EventEmitter from '../shared/EventEmitter.class';
import Loader from '../shared/Loader.class';

import type { DisplayObject } from 'pixi.js';
import type { TRankChangeEvent, TStats } from './Player.class';
import type { TSprites } from '../types';

class Spaceship {
    stats: TStats;
    spaceship: Container;
    bullets: Array<Container>;

    constructor(stats: TStats) {
        this.bullets = [];
        this.stats = stats;
        this.spaceship = new Container();

        // Preload sound effects
        sound.add('shootLaser', {
            url: 'assets/sounds/cartoon_anime_laser_shoot_hard_fast_003_71525.mp3',
            preload: true,
        });
    }

    public get positionX(): DisplayObject['x'] {
        return this.spaceship.position.x;
    }

    public get positionY(): DisplayObject['y'] {
        return this.spaceship.position.y;
    }

    public get shotBullets() {
        return this.bullets;
    }

    private _initListeners() {
        EventEmitter.on('playerRankChanged', ({ stats }: TRankChangeEvent) => {
            this.stats = stats;
        });
    }

    private _initKeyboard() {
        Game.app.ticker.add(() => {
            // X-axis movement
            if (Keyboard.isKeyDown('ArrowLeft', 'KeyA')) {
                if (this.spaceship.x > 0) {
                    this.spaceship.x -= 1 * this.stats.vehicle.velocity;
                }
            } else if (Keyboard.isKeyDown('ArrowRight', 'KeyD')) {
                if (
                    this.spaceship.x <=
                    Game.app.screen.width - this.spaceship.width
                ) {
                    this.spaceship.x += 1 * this.stats.vehicle.velocity;
                }
            }

            // Y-axis movement
            if (Keyboard.isKeyDown('ArrowUp', 'KeyW')) {
                if (this.spaceship.y > 0) {
                    this.spaceship.y -= 1 * this.stats.vehicle.velocity;
                }
            } else if (Keyboard.isKeyDown('ArrowDown', 'KeyS')) {
                if (
                    this.spaceship.y <=
                    Game.app.screen.height - this.spaceship.height
                ) {
                    this.spaceship.y += 1 * this.stats.vehicle.velocity;
                }
            }

            if (Keyboard.isKeyPressed('Space')) {
                this.shootBullet();
                sound.play('shootLaser');
            }

            // Ensure to use this for correct event handling at the end of the game loop.
            Keyboard.update();
        });
    }

    private _addToContainer(sprite: keyof TSprites) {
        if (!Loader.sprites[sprite]) {
            throw new Error('Unknown sprite');
        }
        this.spaceship.addChild(Loader.sprites[sprite]);
    }

    private _addContainerToStage() {
        if (!this.spaceship) {
            throw new Error('Container unavailable');
        }
        Game.app.stage.addChild(this.spaceship);
    }

    private _bulletsLoop() {
        Game.app.ticker.add(() => {
            this._updateBullets();
        });
    }

    private createBullet(cb: (sprite: Container) => void) {
        const container = new Container();

        const bullet = new Sprite(Loader.sprites.bullet_lvl_1.texture);
        bullet.anchor.set(0.5);

        // Shoot the bullet from the center of the player's spaceship
        container.position.x =
            this.positionX + this.spaceship.width / 2 - container.width / 2;
        container.position.y = this.positionY - this.spaceship.height / 2;

        container.addChild(bullet);

        Game.app.stage.addChildAt(container, 1);

        cb(container);
    }

    private _updateBullets() {
        for (let i = 0; i < this.bullets.length; i++) {
            const bullet = this.bullets[i];

            bullet.position.y -= this.stats.weapons.canon.velocity;

            if (bullet.position.y < -bullet.height) {
                this.destroyBulletAt(i);
                return;
            }
        }
    }

    public init() {
        this._initListeners();

        this._addToContainer('player_ship_1');
        this._addContainerToStage();

        // Center the spaceship horizontally
        this.setPositionX(Game.app.screen.width / 2 - this.spaceship.width / 2);
        this.setPositionY(Game.app.screen.height - this.spaceship.height);

        this._initKeyboard();
        this._bulletsLoop();
    }

    public setPositionX(x: DisplayObject['x']) {
        if (!this.spaceship) {
            throw new Error('Sprite unavailable');
        }

        this.spaceship.x = x;
    }

    public setPositionY(y: DisplayObject['y']) {
        if (!this.spaceship) {
            throw new Error('Sprite unavailable');
        }

        this.spaceship.y = y;
    }

    public shootBullet() {
        this.createBullet(bullet => {
            this.bullets.push(bullet);
        });
    }

    public destroyBulletAt(i: number) {
        const bullet = this.bullets[i];
        if (!bullet) {
            throw new Error('Sprite unavailable');
        }
        Game.app.stage.removeChild(bullet);
        this.bullets.splice(i, 1);
    }
}

export default Spaceship;
