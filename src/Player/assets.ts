import type { TAssetMap } from '../types';

const assets: TAssetMap = [
    {
        name: 'player_ship_1',
        url: 'assets/players/playerShip1_blue.png',
    },
    {
        name: 'bullet_lvl_1',
        url: 'assets/bullets/bulletBeige_outline.png',
    },
];

export default assets;
