import { sound } from '@pixi/sound';
import Target from '../Target.class';

class Enemy extends Target {
    public init() {
        super.init('enemyRed1');

        // Preload sound effects
        sound.add('explosion', {
            url: 'assets/sounds/zapsplat_explosion_incoming_whizz_then_big_hit_with_smashing_glass_001_62147.mp3',
            preload: true,
        });
    }

    public destroy(): void {
        sound.play('explosion');
        super.destroy();
    }
}

export default Enemy;
