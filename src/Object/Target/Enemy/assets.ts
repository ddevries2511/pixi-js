import type { TAssetMap } from '../../../types';

const assets: TAssetMap = [
    {
        name: 'enemyRed1',
        url: 'assets/enemies/enemyRed1.png',
    },
];

export default assets;
