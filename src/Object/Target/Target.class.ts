import { Container, Sprite } from 'pixi.js';
import Game from '../../shared/Game.class';
import Loader from '../../shared/Loader.class';

import type { DisplayObject } from 'pixi.js';
import type { TSprites } from '../../types';

class Target {
    public target: Container;
    public isDestroyed = false;

    constructor() {
        this.target = new Container();
    }

    private _addToContainer(sprite: keyof TSprites) {
        if (!Loader.sprites[sprite]) {
            throw new Error('Unknown sprite');
        }
        this.target.addChild(new Sprite(Loader.sprites[sprite].texture));
    }

    private _addContainerToStage() {
        if (!this.target) {
            throw new Error('Container unavailable');
        }
        Game.app.stage.addChild(this.target);
    }

    public init(sprite: keyof TSprites) {
        this._addToContainer(sprite);
        this._addContainerToStage();
    }

    public setPositionX(x: DisplayObject['x']) {
        if (!this.target) {
            throw new Error('Sprite unavailable');
        }

        this.target.x = x;
    }

    public setPositionY(y: DisplayObject['y']) {
        if (!this.target) {
            throw new Error('Sprite unavailable');
        }

        this.target.y = y;
    }

    public get width() {
        return this.target.width;
    }

    public get height() {
        return this.target.height;
    }

    public get positionX() {
        return this.target.position.x;
    }

    public get positionY() {
        return this.target.position.y;
    }

    public destroy() {
        console.log('HIT!!!');

        Game.app.stage.removeChild(this.target);

        // Removes all internal references and listeners as well as removes children from the display list. Do not use a Container after calling destroy.
        this.target.destroy();

        this.isDestroyed = true;
    }
}

export default Target;
