import { Container, Text, TextStyle } from 'pixi.js';
import EventEmitter from '../shared/EventEmitter.class';
import Game from '../shared/Game.class';

class Menu {
    public isStarted = false;

    private startButton: Container;
    private text: Text;
    private textStyle: TextStyle;

    constructor() {
        this.startButton = new Container();
        this.startButton.interactive = true;
        this.startButton.buttonMode = true;

        this.textStyle = new TextStyle({
            fontSize: 36,
            fontWeight: 'bold',
            fill: '#ffffff',
        });
        this.text = new Text('New Game', this.textStyle);

        this.startButton.addChild(this.text);

        Game.app.stage.addChild(this.startButton);

        this.startButton.x =
            Game.app.screen.width / 2 - this.startButton.width / 2;
        this.startButton.y =
            Game.app.screen.height / 2 - this.startButton.height / 2;

        this.startButton.addListener('click', () => {
            this.startGame();
        });
    }

    public startGame() {
        this.isStarted = true;
        this.startButton.destroy();
        EventEmitter.emit('gameStarted');
    }
}

export default Menu;
