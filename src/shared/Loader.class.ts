import { Loader } from 'pixi.js';
import type { TSprites } from '../types';

let shared: GameLoader;

class GameLoader {
    private loader: Loader;

    public sprites: TSprites;

    constructor() {
        this.loader = Loader.shared;

        // Use Pixi's built-in `loader` module to load a sprite
        this.sprites = {};
    }

    private get resources() {
        return this.loader.resources;
    }

    public get shared() {
        return this.loader;
    }
}

/**
 * Returns a shared instance of `GameLoader`
 *
 * @returns GameLoader
 */
const getInstance = () => {
    if (!shared) {
        shared = new GameLoader();
    }

    return shared;
};

export default getInstance();
