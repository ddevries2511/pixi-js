import { utils } from 'pixi.js';

let shared: utils.EventEmitter;

/**
 * Returns a shared instance of `PIXI.utils.EventEmitter`
 *
 * @returns EventEmitter
 */
const getInstance = () => {
    if (!shared) {
        shared = new utils.EventEmitter();
    }

    return shared;
};

export default getInstance();
