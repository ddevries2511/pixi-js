import { Application } from 'pixi.js';
import { resolution } from '../variables';

let shared: Game;
class Game {
    public app: Application;

    constructor() {
        // The application will create a renderer using WebGL, if possible,
        // with a fallback to a canvas render. It will also setup the ticker
        // and the root stage PIXI.Container.
        this.app = new Application({
            resolution,
            resizeTo: window,
        });

        this.init();
    }

    /**
     * init
     */
    public init() {
        // The application will create a canvas element for you that you
        // can then insert into the DOM.
        document.body.appendChild(this.app.view);
    }
}

/**
 * Returns a shared instance of `Game`
 *
 * @returns Game
 */
const getInstance = () => {
    if (!shared) {
        shared = new Game();
    }

    return shared;
};

export default getInstance();
