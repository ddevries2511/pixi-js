# Divotion Hackathon 2022 - PixiJS

## Usage

Install dependencies:

```bash
$ npm ci
```

---

Start development server:

```bash
$ npm start
```

The development server runs on port `9000` by default.

You can change it in `webpack.config.ts`, e.g.:

```json
devServer: {
    port: 80,
}
```

---

Build for production:

```bash
$ npm run prebuild && npm run build
```

---

Run ESLint:

```bash
$ npm run lint
```

or

```bash
$ npm run lint:fix
```

---

## PixiJS - The HTML5 Creation Engine

Create beautiful digital content with the fastest, most flexible 2D WebGL renderer.

---

### Usefull Links

-   [PixiJS - Getting Started](https://pixijs.io/guides/basics/getting-started.html)
-   [PixiJS - API Docs](https://pixijs.io/docs)
-   [PixiJS - Sound Docs](https://pixijs.io/sound/examples/)
-   [PixiJS - Examples](https://pixijs.io/examples/)
-   [Kenney - Free game assets](https://kenney.nl/assets)
-   [GAMETEXTURES](https://gametextures.com/)
-   [Tiled Map Editor](https://www.mapeditor.org/)
-   [Zapsplat Sound FX](https://www.zapsplat.com/)

---

### About this project

Leuk dat je meedoet met de Divotion Hackathon!

Dit is een voorbeeld project waarin de basis concepten van `PixiJS` worden gebruikt. Hetgeen wat je aan code hier terugvindt is daarom ook zeker niet alles wat je ermee kan doen!

Je kunt dit project gebruiken om snel een werkend project en bestandstructuur klaar te zetten en voorbeeld te nemen aan de opzet.

Natuurlijk is er veel te veel informatie om in één README kwijt te kunnen, maar hopelijk kom je hiermee aardig op weg.

Veel plezier!

[Live preview](https://pixijs.by-dennis.nl)

---

### Basic Concepts

Hier zal ik de basis concepten van `PixiJS` en hun implementatie in dit project kort toelichten. Meer informatie kun je terugvinden in de [Guides](https://pixijs.io/guides/basics/getting-started.html), [API Docs](https://pixijs.io/docs) en [Voorbeelden](https://pixijs.io/examples/).

---

#### Application

Ieder simpel `PixiJS` project begint met een [Application](https://pixijs.download/release/docs/PIXI.Application.html). Het creëert de `renderer`, `stage` en `ticker`. Je kunt aan deze `class` meerdere opties meegeven m.b.t. resolutie, schaal, etc.

In dit project wordt er een herbruikbare instantie van `Application` gecreëerd en toegevoegd aan de `DOM` in `shared/Game.class.ts`.

```ts
const app = new Application();
```

---

#### Sprites

De simpelste manier om afbeeldingen te weergeven is via het gebruik van een [Sprite](https://pixijs.download/release/docs/PIXI.Sprite.html).

Voordat een `Sprite` weergeven kan worden, moet deze eerst ingeladen worden.
Dat kan d.m.v.:

```ts
const sprite = PIXI.Sprite.from('sample.png');
```

Deze kun je daarna toevoegen aan de `stage`.

```ts
app.stage.addChild(sprite);
```

Een efficientere manier om `sprites` in te laden is met `PIXI.Loader`.
In dit project wordt er een herbruikbare instantie van de `Loader` gecreëerd in `shared/Loader.class.ts`. In de `Loader` worden `sprites` en hun `textures` opgeslagen in `cache` en kun je deze hergebruiken.

Voorbeeld uit `index.ts`:

```ts
const loader = new PIXI.loader();

// Load all assets
const assets: TAssetMap = [...playerAssets, ...enemyAssets];
for (const asset of assets) {
    loader.shared.add(asset);
}

// resources is an object where the key is the name of the resource loaded and the value is the resource object.
// They have a couple default properties:
// - `url`: The URL that the resource was loaded from
// - `error`: The error that happened when trying to load (if any)
// - `data`: The raw data that was loaded
// also may contain other properties based on the middleware that runs.
loader.shared.load((_loader, resources) => {
    for (const key in resources) {
        const resource = resources[key];
        loader.sprites[key] = new Sprite(resource.texture);
    }
});

loader.shared.onComplete.add(() => {
    console.info('Assets loaded', loader.sprites);
});
```

---

#### Update Loop

De `ticker` kun je gebruiken om animaties en logica toe te voegen aan je `PixiJS` applicatie. De callback die je meegeeft aan `app.ticket.add(...)` wordt iedere frame uitgevoerd. In dit project wordt de `ticker` gebruikt voor [_collision detection_](https://pixijs.io/examples/#/demos-advanced/collision-detection.js) tussen kogels en vijanden.

Voorbeeld uit [PixiJS - Getting Started Guide](https://pixijs.io/guides/basics/getting-started.html):

```ts
// Add a variable to count up the seconds our demo has been running
let elapsed = 0.0;
// Tell our application's ticker to run a new callback every frame, passing
// in the amount of time that has passed since the last tick
app.ticker.add(delta => {
    // Add the time to our total elapsed time
    elapsed += delta;
    // Update the sprite's X position based on the cosine of our elapsed time.
    // We divide by 50 to slow the animation down a bit...
    sprite.x = 100.0 + Math.cos(elapsed / 50.0) * 100.0;
});
```

---

#### Tilemaps

[Tiled Map Editor](https://thorbjorn.itch.io/tiled) is een applicatie (Windows, Mac, Linux) die je kunt gebruiken om `TileMap`s mee te maken.
`TileMap`s kun je gebruiken om bijvoorbeeld een decor voor je game te maken en kunnen zelfs geanimeerd zijn!

[Tiled Map Editor - Getting started](https://doc.mapeditor.org/en/stable/manual/introduction/#getting-started)

Nadat je een `TileMap` gemaakt hebt kun je deze in je `PixiJS` applicatie [importeren](https://pixijs.io/examples/#/tilemaps/basic.js).

```ts
const renderer = PIXI.autoDetectRenderer({
    antialias: true,
    autoDensity: true,
    resolution: window.devicePixelRatio || 1,
    width: 800,
    height: 600,
});

let stage;
let tilemap;
let frame = 0;

document.body.appendChild(renderer.view);

const loader = new PIXI.Loader();

loader.add('atlas', 'examples/assets/tilemaps/atlas.json');
loader.add('button', 'examples/assets/tilemaps/button.png');
loader.load((_, resources) => {
    // Setup tilemap scene
    stage = new PIXI.Container();
    tilemap = new PIXI.tilemap.CompositeTilemap();
    stage.addChild(tilemap);

    // Setup rendering loop
    PIXI.Ticker.shared.add(() => renderer.render(stage));

    makeTilemap();
    setInterval(() => {
        // Animate the chest tile textures. Since they are placed in 1 row
        // only, we only need to update tileAnim[0] (for x) and not
        // tileAnim[1] (for y).
        renderer.plugins.tilemap.tileAnim[0] = frame++;
    }, 400);
});

function makeTilemap() {
    // Clear the tilemap, in case it is being reused.
    tilemap.clear();

    const resources = loader.resources;
    const size = 32;

    // Calculate the dimensions of the tilemap to build
    const pxW = renderer.screen.width;
    const pxH = renderer.screen.height;
    const tileW = pxW / size;
    const tileH = pxH / size;
    const wallBoundary = 2 + Math.floor(tileH / 2);

    // Fill the scene with grass and sparse rocks on top and chests on
    // the bottom. Some chests are animated between two tile textures
    // (so they flash red).
    for (let i = 0; i < tileW; i++) {
        for (let j = 0; j < tileH; j++) {
            tilemap.tile(
                j < tileH / 2 && i % 2 === 1 && j % 2 === 1
                    ? 'tough.png'
                    : 'grass.png',
                i * size,
                j * size
            );

            if (j === wallBoundary) {
                tilemap.tile('brick_wall.png', i * size, j * size);
            } else if (j > wallBoundary + 1 && j < tileH - 1) {
                if (Math.random() > 0.8) {
                    tilemap.tile('chest.png', i * size, j * size);

                    if (Math.random() > 0.8) {
                        // Animate between 2 tile textures. The x-offset
                        // between them in the base-texture is 34px, i.e.
                        // "red_chest" is exactly 34 pixels right in the atlas.
                        tilemap.tileAnimX(34, 2);
                    }
                }
            }
        }
    }

    // Button does not appear in the atlas, but @pixi/tilemap won't surrender
    // - it will create second layer for special for buttons and they will
    // appear above all the other tiles.
    tilemap.tile(resources.button.texture, 0, 0);
}
```

---

#### Interaction

Ieder [PIXI.DisplayObject](https://pixijs.download/release/docs/PIXI.DisplayObject.html) kan interactief worden gemaakt door simpelweg zijn `interactive` property op `true` te zetten. Hierdoor zal het object `interaction events` uitzenden die je kunt afvangen.

```ts
const app = new PIXI.Application({ backgroundColor: 0x1099bb });
document.body.appendChild(app.view);

// Scale mode for all textures, will retain pixelation
PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

const sprite = PIXI.Sprite.from('examples/assets/bunny.png');

// Set the initial position
sprite.anchor.set(0.5);
sprite.x = app.screen.width / 2;
sprite.y = app.screen.height / 2;

// Opt-in to interactivity
sprite.interactive = true;

// Shows hand cursor
sprite.buttonMode = true;

// Pointers normalize touch and mouse
sprite.on('pointerdown', onClick);

// Alternatively, use the mouse & touch events:
// sprite.on('click', onClick); // mouse-only
// sprite.on('tap', onClick); // touch-only

app.stage.addChild(sprite);
```

---

##### Interaction using the keyboard

Standaard biedt `PixiJS` geen ondersteuning voor toetsenbord interactie.
Hiervoor wordt in dit project de [pixi.js-keyboard](https://www.npmjs.com/package/pixi.js-keyboard) libaray gebruikt.

Voorbeeld uit `Player/Spaceship.class.ts`:

```ts
Game.app.ticker.add(() => {
    // X-axis movement
    if (Keyboard.isKeyDown('ArrowLeft', 'KeyA')) {
        // Do something
    } else if (Keyboard.isKeyDown('ArrowRight', 'KeyD')) {
        // Do something
    }

    // Y-axis movement
    if (Keyboard.isKeyDown('ArrowUp', 'KeyW')) {
        // Do something
    } else if (Keyboard.isKeyDown('ArrowDown', 'KeyS')) {
        // Do something
    }

    if (Keyboard.isKeyPressed('Space')) {
        // Do something
    }

    // Ensure to use this for correct event handling at the end of the game loop.
    Keyboard.update();
});
```

---

#### EventEmitter

Je kunt `PIXI` objecten ook `custom events` laten uitzenden. Dit kan handig zijn als je andere objecten wilt laten weten dat er iets is gebeurd.
In dit project wordt er een herbruikbare instantie van de `EventEmitter` gecreëerd in `shared/EventEmitter.class.ts`.

```ts
// Emit a custom event with payload

const payload = {
    message: 'Custom event message',
};

EventEmitter.emit('customEvent', payload);
```

```ts
// Listen to a custom event

EventEmitter.on('customEvent', payload => {
    // Do something
});
```

---

#### Sound FX

Je kunt de `PIXI.Loader` ook gebruiken om een geluidseffecten in te laden en af te spelen:

```ts
PIXI.Loader.shared.add('bird', 'resources/bird.mp3');
PIXI.Loader.shared.load(function (loader, resources) {
    resources.bird.sound.play();
});
```

Voorbeeld uit `Player/Player.class.ts`:

```ts
import { sound } from '@pixi/sound';

class Player {
    constructor() {
        // Preload sound effects
        sound.add('lvlUp', {
            url: 'assets/sounds/PM_FN_Events_LvlUps_PowerUps_56.mp3',
            preload: true,
        });
    }

    public set playerRank(rank: TRank) {
        this.rank = rank;
        // Play 'level up' sound effect
        sound.play('lvlUp');
    }
}
```
